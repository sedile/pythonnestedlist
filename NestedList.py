class Element:
    
    def __init__(self, name, value):
        self.__pair = (name,value)
        
    def getID(self):
        return self.__pair[0]
    
    def getValue(self):
        return self.__pair[1]
    
    def getElement(self):
        return self.__pair
    
    def getElementAsString(self):
        elem = str(self.__pair)
        elem = elem.replace("'","")
        elem = elem.replace(",","")
        return elem

class NestedList:
    
    def __init__(self):
        self.__nlist = []  
        
    def parseNestedList(self, nlist):
        temp = []
        if not nlist[0] == "(":
            return ""
        else:
            brackets = 0
            for idx in range(1, len(nlist)):
                if nlist[idx] == "(":
                    brackets += 1
                    if brackets == 1:
                        start = idx
                elif nlist[idx] == ")":
                    brackets -= 1
                    if brackets == 0:
                        temp.append(nlist[start:idx+1])
        self.__builtNestedList(temp)
        
    def __builtNestedList(self, templist):
        self.__nlist.clear()
        for elem in templist:
            name = ""
            value = ""
            for i in range(1, len(elem)):
                if elem[i] == ' ':
                    name = elem[1:i]
                    value = elem[i+1:len(elem)-1]
                    self.__nlist.append(Element(name,value))
                    break                   
                    
    def importNestedList(self, listfile):
        file = open(listfile,'r')
        content = ""
        for line in file:
            content += line
        self.parseNestedList(content)
        print('nestedlist load successfully')
            
    def exportNestedList(self, path):
        file = open(path,'w')
        file.write(self.getNestedListAsString())
        print('nestedlist saved in ' + path)
        
    def addElement(self, name, value):
        self.__nlist.append(Element(name, value))
        
    def addListElements(self, name, listvalues):
        self.__nlist.append(Element(name, listvalues))

    def addTupleElements(self, name, tuplevalues):
        self.__nlist.append(Element(name, tuplevalues))

    def first(self):
        if len(self.__nlist) >= 1:
            return self.__nlist[0]
        else:
            return []
    
    def second(self):
        if len(self.__nlist) >= 2:
            return self.__nlist[1]
        else:
            return []
    
    def third(self):
        if len(self.__nlist) >= 3:
            return self.__nlist[2]
        else:
            return []
    
    def fourth(self):
        if len(self.__nlist) >= 4:
            return self.__nlist[3]
        else:
            return []
    
    def fifth(self):
        if len(self.__nlist) >= 5:
            return self.__nlist[4]
        else:
            return []
    
    def getElementByID(self, element_id):
        for elem in self.__nlist:
            if elem.getID() == element_id:
                return elem
        return []
    
    def getSize(self):
        return len(self.__nlist)
    
    def isEmpty(self):
        if self.getSize() == 0:
            return True
        else:
            return False
        
    def intValue(self, value):
        return int(value)
    
    def floatValue(self, value):
        return float(value)
    
    def boolValue(self, value):
        return bool(value)
    
    def listValue(self, value):
        str_to_list = []
        start = 1
        for i in range(1, len(value)):
            if value[i] == ' ' or value[i] == ')':
                str_to_list.append(value[start:i])
                start = i + 1
        return str_to_list
    
    def getNestedList(self):
        return self.__nlist
    
    def getNestedListAsString(self):
        nl_string = "("
        for i in self.__nlist:
            nl_string += i.getElementAsString()
        nl_string += ")"
        nl_string = nl_string.replace('[', '(')
        nl_string = nl_string.replace(']', ')')
        return nl_string