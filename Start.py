from NestedList import NestedList

if __name__ == "__main__":

    #create an empty nested list
    nlist = NestedList()

    #create some data
    list = ['a','b','c','d','e']
    tuple = (2,3,5,7,11,13,17,19)
    
    # add data to the nested list
    nlist.addElement("autohaus", "koch")
    nlist.addElement("PI", 3.1415)
    nlist.addListElements("buchstabenliste", list)
    nlist.addTupleElements("primzahlen_10", tuple)
    
    print(nlist.getNestedListAsString())
    
    # save list
    nlist.exportNestedList("/.../test.nl")
    
    # load a stored nested list
    nlist.importNestedList("/.../test.nl")

    # get one element and cast it to a specific type
    elem = nlist.second().getValue()
    elem = nlist.floatValue(elem) + - 0.1415
    print(elem)
    
    # get a specific element
    le = nlist.getElementByID('primzahlen_10').getValue()
    print(nlist.listValue(le))
    
    print(nlist.getNestedListAsString())